#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

typedef struct puntos{
	int jugador;
	int puntos;
};

void imprimePuntos(puntos a){
	cout << "El jugador " << a.jugador << " ha marcado 1 punto, lleva un total de " << a.puntos << "puntos" << endl;
}


int main(int argc, char **argv) {
	int fd;
	int puntos1, puntos2;
	puntos p;

	//Creamos el FIFO
	if (mkfifo("FIFO", 0600) < 0){
		perror("No puede crearse el FIFO");
		return(1);
	}

	//Abrimos el FIFO en modo lectura para el caso del logger
	if ((fd = open("FIFO", O_RDONLY)) < 0){
		perror("No puede abrirse el FIFO");
		return(1);
	}

	while ((read(fd, &p, sizeof(p))) == sizeof(p)){
		imprimePuntos(p);
	}

	close (fd);
	unlink("FIFO");
	return(0);
}