#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#include "DatosMemCompartida.h"

int main(int argc, char **argv){
	DatosMemCompartida *datos;
	int fd_fich;
	char a;

	//Abrimos el fichero correspondiente
	if (fd_fich = open("DATOS", O_RDWR, 0777) < 0){
		perror("No puede abrirse el fichero");
		return(1);
	}

	//Proyectamos el fichero
	if ((datos = (DatosMemCompartida*)mmap(NULL, sizeof(datos), PROT_READ, MAP_ANON|MAP_SHARED, fd_fich, 0)) 
		== MAP_FAILED){
        perror("Error en la proyeccion del fichero");
    	cout << errno << endl;
        close(fd_fich);
    }

    //***************IMPORTANTE******************
    //Se necesita una conversion forzada para que funcione el mmap

    close(fd_fich);

    //Se crea el bucle infinito que hace que el bot se ejecute
    while (1){
    	if (datos->esfera.centro.y > datos->raqueta1.y1) 			cout << datos->esfera.centro.y << "\t" << datos->raqueta1.y1 <<"\t arriba" << endl;
    	else if (datos->esfera.centro.y == datos->raqueta1.y1)		cout << datos->esfera.centro.y << "\t" << datos->raqueta1.y1 <<"\t centro" << endl;
    	else if (datos->esfera.centro.y < datos->raqueta1.y1)		cout << datos->esfera.centro.y << "\t" << datos->raqueta1.y1 <<"\t abajo" << endl;
    	usleep(25000);
    }

    unlink("DATOS");
    return(0);
}