// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <iostream>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//unlink("DATOS");
	close(fd_sc);
	unlink("fifosc");

	close(fd_client);
	unlink("FIFOcliente");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	/*jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.radio -= 0.0008f;
	if (esfera.radio <= 0.1f) esfera.radio = 0.1f;
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		m = {2, puntos2};
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		m = {1, puntos1};
	}*/

	//A continuación se va a actualizar los datos de memoria compartida
	/*datos->esfera = esfera;
	datos->raqueta1 = jugador1;*/


	//Se le da el control al bot
	/*if (datos->accion == 1) 		CMundo::OnKeyboardDown('w', 0, 0);
	else if (datos->accion == 0)	CMundo::OnKeyboardUp('w', 0, 0);
	else if (datos->accion == -1)	CMundo::OnKeyboardDown('s', 0, 0);*/
	/********COMENTADO PORQUE NO FUNCIONA***********/

	read(fd_sc, cad, sizeof(cad));
	sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2,
		&jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key) {
	case 's':
		sprintf(info_tecla, )
		break;
	case 'w':
		jugador1.velocidad.y=4;
		break;
	case 'l':
		jugador2.velocidad.y=-4;
		break;
	case 'o':
		jugador2.velocidad.y=4;
		break;
	case'c':
		Disparo1 = new Esfera(((jugador1.x1 + jugador1.x2)/2 + 3), ((jugador1.y1 + jugador1.y2)/2));
		break;
	case 'm':
		Disparo2 = new Esfera(((jugador2.x1 + jugador2.x2)/2 + 3), ((jugador2.y1 + jugador2.y2)/2));
		break;
	}*/
	sprintf(info_tecla, "%c d", key);
	write(fd_client, &info_tecla, sizeof(info_tecla));
}

void CMundo::OnKeyboardUp(unsigned char key, int x, int y){
	/*switch(key){
		case 's': 
			jugador1.velocidad.y = 0;
			break;
		case 'w':
			jugador1.velocidad.y = 0;
			break;
		case 'l':
			jugador2.velocidad.y = 0;
			break;
		case 'o':
			jugador2.velocidad.y = 0;
			break;
	}*/
	sprintf(info_tecla, "%c u", key);
	write(fd_client, &info_tecla, sizeof(info_tecla));
}

void CMundo::Init()
{
	DatosMemCompartida aux;

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Creamos el fichero de datos en modo escritura
	/*if ((fd_fich = open("./DATOS", O_CREAT|O_RDWR, 0777)) < 0){
		perror("No puede abrirse el fichero");
		return;
	}

	//Establece la longitud del fichero 
    if (ftruncate(fd_fich, sizeof(aux))) {
        perror("Error en ftruncate del fichero");
        close(fd_fich);
        return;
    }

    //Se proyecta el fichero en memoria
    if ((datos = (DatosMemCompartida*)mmap(NULL, sizeof(aux), PROT_READ|PROT_WRITE, MAP_SHARED, fd_fich, 0))
    	== MAP_FAILED){
        perror("Error en la proyeccion del fichero");
        close(fd_fich);
        return;
    }

    close(fd_fich);*/

	//Creamos el FIFO para transmitir la informacion servidor->cliente y lo abrimos
    if (mkfifo("fifosc", 0600) < 0){
    	perror("El FIFO de comunicacion servidor->cliente ha fallado al crearse.");
    	return;
    }

    if ((fd_sc = open("fifosc", O_RDONLY)) < 0){
    	perror("Error. No se ha podido abrir el FIFO de comunicacion servidor->cliente.");
    	return;
    }

    //Creamos la tuberia para transmitir la informacion cliente->servidor y lo abrimos
    if (mkfifo("FIFOcliente", 0777) < 0){
    	perror("El FIFO de comunicacion cliente->servidor ha fallado al crearse.");
    	return;
    }

    if ((fd_client = open("FIFOcliente", O_WRONLY)) < 0){
    	perror("Error. No se ha podido abrir el FIFO de comunicacion cliente->servidor.");
    	return;
    }
}