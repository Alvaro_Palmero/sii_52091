// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"
//No incluimos Raqueta.h y Esfera.h porque estan incluidos en DatosMem.. y supondria redeclaraciones

//Creamos una estructura para enviar los datos mediante la tuberia FIFO
typedef struct mensaje {
	int jugador;
	int puntos;
};

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnKeyboardUp(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	Esfera* Disparo1;
	Esfera* Disparo2;

	//Declaramos el fd para el FIFO y la estructura del mensaje
	int fd;
	mensaje m;

	//Declaramos la estructura de datos compartidos
	int fd_fich;
	DatosMemCompartida* datos;

	int puntos1;
	int puntos2;

	//Declaramos las variables necesarias para la comunicacion servidor->cliente
	char cad[200];
	int fd_sc;

	//Declaramos una variable de tipo thread
	pthread_t thid1;
	int fd_client;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
